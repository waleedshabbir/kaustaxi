import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Storage } from "@ionic/storage";
import { GlobalVariables } from "../../app/gloabls";
import { EmailValidator } from "../../validators/email";
import { EmailService } from "../../providers/email-provider";

@IonicPage()
@Component({
  selector: 'page-taxi-form',
  templateUrl: 'taxi-form.html',
})
export class TaxiFormPage {

  submitAttempt: boolean = false;
  taxiForm: FormGroup;
  testString = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public formBuilder: FormBuilder,
    public globals: GlobalVariables,
    private es: EmailService,
    private storage: Storage) {

    this.taxiForm = formBuilder.group({
      tripWay: ['One Way', Validators.required],
      title: [this.globals.user.title, Validators.required],
      phone: [this.globals.user.phone, Validators.pattern('[0-9]*')],
      email: [this.globals.user.email, Validators.compose([Validators.required, EmailValidator.isValid])],
      pickUpPlace: ['', Validators.required],
      pickUpGoogleMapLink: [''],
      pickUpTime: ['', Validators.required],
      vehicleType: ['Med-Sedan', Validators.required],
      serviceProvider: ['Samara Transport', Validators.required],
      name: [this.globals.user.name,
      Validators.compose([Validators.required, Validators.pattern('[a-zA-Z ]*'), Validators.maxLength(15)])],
      mobile: [this.globals.user.mobile,
      Validators.compose([Validators.required, Validators.pattern('[0-9]*'), Validators.maxLength(11)])],
      kaustId: ['', Validators.required],
      destination: ['', Validators.required],
      destGoogleMapLink: [''],
      date: ['', Validators.required],
      noOfPassengers: ['1', Validators.required],
      shareThisRide: ['Yes', Validators.required],
      additionalInstructions: ['']
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TaxiFormPage');
  }

  async submit() {
    this.submitAttempt = true
    if (!this.taxiForm.valid) {
      console.error("bad shit")
    } else {
      console.log("this.taxiForm.value", this.taxiForm.value);
      let loader = this.loadingCtrl.create({
        content: "Submitting form please wait",
        dismissOnPageChange: true
      });
      loader.present()
      try {
        let formValues = this.configureFormValues();
        let sendingEmailTo = this.taxiForm.value.serviceProvider == 'Samara Transport' ?
          'samaratransport@kaust.edu.sa' : 'hancotransport@kaust.edu.sa'
        const resp = await this.es.sendEmail(formValues, sendingEmailTo)
        console.log("rep", resp)
        let alert = this.alertCtrl.create({
          title: 'Success',
          subTitle: resp.message,
          buttons: ["OK"]
        })
        alert.present()
        this.navCtrl.pop()
      } catch (error) {
        loader.dismiss()
        console.error("error", error)
        let alert = this.alertCtrl.create({
          title: 'Error',
          subTitle: error.message,
          buttons: ["OK"]
        })
        alert.present()
      }
    }
  }

  configureFormValues() {
    let formValues = {
      'Trip Way': this.taxiForm.value.tripWay,
      'Title': this.taxiForm.value.title,
      'Phone': this.taxiForm.value.phone,
      'Email': this.taxiForm.value.email,
      'Pickup Place': this.taxiForm.value.pickupPlace,
      'Pickup Google Map Link': this.taxiForm.value.pickUpGoogleMapLink,
      'Pickup Time': this.taxiForm.value.pickupTime,
      'Vehicle Type': this.taxiForm.value.vehicleType,
      'Service Provider': this.taxiForm.value.serviceProvider,
      'Name': this.taxiForm.value.name,
      'Mobile': this.taxiForm.value.mobile,
      'Kaust ID': this.taxiForm.value.kaustId,
      'Destination': this.taxiForm.value.destination,
      'Destination GoogleMap Link': this.taxiForm.value.destGoogleMapLink,
      'Date': this.taxiForm.value.date,
      'No. Of Passengers': this.taxiForm.value.noOfPassengers,
      'Share This Ride': this.taxiForm.value.shareThisRide,
      'Additional Instructions': this.taxiForm.value.additionalInstructions
    }
    return formValues;
  }

}
