import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TaxiFormPage } from './taxi-form';

@NgModule({
  declarations: [
    TaxiFormPage,
  ],
  imports: [
    IonicPageModule.forChild(TaxiFormPage),
  ],
})
export class TaxiFormPageModule {}
