import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { EmailProvider } from "../../providers/email/email";
import { EmailService } from "../../providers/email-provider";

@IonicPage()
@Component({
  selector: 'page-landing',
  templateUrl: 'landing.html',
})
export class LandingPage {

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    private es: EmailService,
    public emailProvider: EmailProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LandingPage');
  }

  async taxiForm() {
    this.load()
    this.navCtrl.push('TaxiFormPage')
  }

  async customerSurver() {
    await this.load()
    this.navCtrl.push('CustomerSurveyFormPage')
  }

  async userInfo() {
    await this.load()
    this.navCtrl.push('UserInfoPage')
  }

  async sendEmail() {
    try {
      const gridEmail = await this.es.sendEmail()
      console.log("grid", gridEmail)
    } catch (error) {
      console.error("grid error", error)
    }
  }

  load() {
    let loader = this.loadingCtrl.create({
      dismissOnPageChange: true
    })
    return loader.present()
  }
}
