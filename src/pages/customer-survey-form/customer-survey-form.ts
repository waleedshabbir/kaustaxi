import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { FormControl, FormGroup, FormBuilder, Validators } from "@angular/forms";
import { EmailService } from "../../providers/email-provider";
import { GlobalVariables } from "../../app/gloabls";


@IonicPage()
@Component({
  selector: 'page-customer-survey-form',
  templateUrl: 'customer-survey-form.html',
})
export class CustomerSurveyFormPage {

  submitAttempt: boolean = false;
  surveyForm: FormGroup;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public globals: GlobalVariables,
    public formBuilder: FormBuilder,
    private es: EmailService) {


    this.surveyForm = formBuilder.group({
      name: [this.globals.user.name],
      busService: ['', Validators.required],
      taxiService: ['', Validators.required],
      busAndTaxiServices: formBuilder.group({
        taxiInCampus: ['', Validators.required],
        taxiOffCampus: ['', Validators.required],
        busInCampus: ['', Validators.required],
        busOffCampus: ['', Validators.required],
      }),
      customerSatisfaction: formBuilder.group({
        behaviour: ['', Validators.required],
        taxiTimming: ['', Validators.required],
        busTimming: ['', Validators.required],
        busCleanliness: ['', Validators.required],
        taxiCleanliness: ['', Validators.required],
        busTemperature: ['', Validators.required],
        locationOfStops: ['', Validators.required],
        bookingWebsites: ['', Validators.required],
        customerService: ['', Validators.required],
        efficiency: ['', Validators.required],
        timeCommitment: ['', Validators.required],
        busSafety: ['', Validators.required],
        taxiSafety: ['', Validators.required],
      }),
      taxiProviderPreference: formBuilder.group({
        hanco: [false],
        samara: [false]
      }),
      busPurpose: formBuilder.group({
        businessUse: [false],
        withinKAUST: [false],
        shopping: [false],
        airport: [false],
        medical: [false]
      }),
      taxiPuspose: formBuilder.group({
        businessUse: [false],
        withinKAUST: [false],
        shopping: [false],
        airport: [false],
        medical: [false]
      }),
      taxiTrip: [''],
      comments: ['']
    })

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CustomerSurveyFormPage');
  }

  async submit() {
    let loader = this.loadingCtrl.create({
      content: "Submitting form",
      dismissOnPageChange: true
    });
    loader.present()
    this.submitAttempt = true
    if (!this.surveyForm.valid) {
      console.log(this.surveyForm.value)
      loader.dismiss()
    } else {
      console.log("this.surveyForm.value", this.surveyForm.value)
      let formValues = this.configureFormValues()
      try {
        const resp = await this.es.sendEmail(formValues, 'transport.compliance@kaust.edu.sa')
        console.log("rep", resp)
        let alert = this.alertCtrl.create({
          title: 'Success',
          subTitle: resp.message,
          buttons: ["OK"]
        })
        alert.present()
        this.navCtrl.pop()
      } catch (error) {
        console.error("error", error);
        let alert = this.alertCtrl.create({
          title: 'Error',
          subTitle: error.message,
          buttons: ["OK"]
        })
        alert.present()
      }
    }
  }

  configureFormValues() {
    let formValues = {
      'Name': this.surveyForm.value.name,
      'Bus Service': this.surveyForm.value.busService,
      'Taxi Service': this.surveyForm.value.taxiService,
      'Taxi In Campus': this.surveyForm.value.busAndTaxiServices.taxiInCampus,
      'Taxi Off Campus': this.surveyForm.value.busAndTaxiServices.taxiOffCampus,
      'Bus In Campus': this.surveyForm.value.busAndTaxiServices.busInCampus,
      'Bus Off Campus': this.surveyForm.value.busAndTaxiServices.busOffCampus,
      'Behaviour': this.surveyForm.value.customerSatisfaction.behaviour,
      'Taxi Timming': this.surveyForm.value.customerSatisfaction.taxiTimming,
      'Bus Timming': this.surveyForm.value.customerSatisfaction.busTimming,
      'Bus Cleanliness': this.surveyForm.value.customerSatisfaction.busCleanliness,
      'Taxi Cleanliness': this.surveyForm.value.customerSatisfaction.taxiCleanliness,
      'Bus Temperature': this.surveyForm.value.customerSatisfaction.busTemperature,
      'Location Of Stops': this.surveyForm.value.customerSatisfaction.locationOfStops,
      'Booking Websites': this.surveyForm.value.customerSatisfaction.bookingWebsites,
      'Customer Service': this.surveyForm.value.customerSatisfaction.customerService,
      'Efficiency': this.surveyForm.value.customerSatisfaction.efficiency,
      'Time Commitment': this.surveyForm.value.customerSatisfaction.timeCommitment,
      'Bus Safety': this.surveyForm.value.customerSatisfaction.busSafety,
      'Taxi Safety': this.surveyForm.value.customerSatisfaction.taxiSafety,
      'Hanco': this.surveyForm.value.taxiProviderPreference.hanco,
      'Samara': this.surveyForm.value.taxiProviderPreference.samara,
      'Bus Use': this.surveyForm.value.busPurpose.businessUse,
      'Bus Within KAUST': this.surveyForm.value.busPurpose.withinKAUST,
      'Bus Shopping': this.surveyForm.value.busPurpose.shopping,
      'Bus Airport': this.surveyForm.value.busPurpose.airport,
      'Bus Medical': this.surveyForm.value.busPurpose.medical,
      'Taxi Business Use': this.surveyForm.value.taxiPuspose.businessUse,
      'Taxi Within KAUST': this.surveyForm.value.taxiPuspose.withinKAUST,
      'Taxi Shopping': this.surveyForm.value.taxiPuspose.shopping,
      'Taxi Airport': this.surveyForm.value.taxiPuspose.airport,
      'Taxi Medical': this.surveyForm.value.taxiPuspose.medical,
      'Taxi Trip': this.surveyForm.value.taxiTrip,
      'Comments': this.surveyForm.value.comments
    }
    console.log(formValues)
    return formValues;
  }

}
