import { NgModule } from '@angular/core';
import { IonicPageModule, App, IonicApp } from 'ionic-angular';
import { CustomerSurveyFormPage } from './customer-survey-form';

@NgModule({
  declarations: [
    CustomerSurveyFormPage,
  ],
  imports: [
    IonicPageModule.forChild(CustomerSurveyFormPage),
  ],
})
export class CustomerSurveyFormPageModule {}
