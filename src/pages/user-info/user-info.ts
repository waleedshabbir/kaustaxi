import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { FormControl, FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Storage } from "@ionic/storage";
import { GlobalVariables } from "../../app/gloabls";
import { EmailValidator } from "../../validators/email";


@IonicPage()
@Component({
  selector: 'page-user-info',
  templateUrl: 'user-info.html',
})
export class UserInfoPage {

  submitAttempt: boolean = false;
  userInfoForm: FormGroup;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public formBuilder: FormBuilder,
    public globals: GlobalVariables,
    private storage: Storage) {

    this.userInfoForm = formBuilder.group({
      title: [this.globals.user.title, Validators.required],
      name: [this.globals.user.name,
      Validators.compose([Validators.required, Validators.pattern('[a-zA-Z ]*'), Validators.maxLength(15)])],
      mobile: [this.globals.user.mobile,
      Validators.compose([Validators.required, Validators.pattern('[0-9]*'), Validators.maxLength(11)])],
      phone: [this.globals.user.phone, Validators.pattern('[0-9]*')],
      email: [this.globals.user.email, Validators.compose([Validators.required, EmailValidator.isValid])]
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UserInfoPage');
  }

  async submit() {
    this.submitAttempt = true;
    if (!this.userInfoForm.valid) {
    } else {
      let loader = this.loadingCtrl.create({
        content: 'Saving Information',
        dismissOnPageChange: true
      })
      loader.present()
      try {
        await this.storage.set('user', this.userInfoForm.value)
        const user = await this.storage.get('user');
        this.globals.user.email = user.email;
        this.globals.user.title = user.title;
        this.globals.user.name = user.name;
        this.globals.user.mobile = user.mobile;
        this.globals.user.phone = user.phone;
        console.log('user', user);
        console.log('title', user.title);
        this.navCtrl.pop()
      } catch (error) {
        console.error("error", error);
      }
    }
  }
}
