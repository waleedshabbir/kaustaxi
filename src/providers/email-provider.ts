import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';


@Injectable()
export class EmailService {

  constructor(public http: Http) {
    console.log('Hello PaymentService Provider');
  }

  // sendEmail(formValues?, sendEmailTo?) {
  //   let url = "https://frozen-journey-24504.herokuapp.com/api/test_sendgrid"
  //   let body = new URLSearchParams();
  //   body.set("email", 'waleedshabbir1@gmail.com');
  //   body.set("formValues", JSON.stringify(formValues))

  //   let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
  //   let options = new RequestOptions({ headers: headers });
  //   console.log(body);

  //   return this.http.post(url, body, options)
  //     .toPromise()
  //     .then(this.extractData)
  //     .catch(this.catchError);
  // }

  sendEmail(formValues?, sendEmailTo?) {
    let url = "https://frozen-journey-24504.herokuapp.com/api/test_sendgrid"
    let body = new URLSearchParams();
    if (formValues['Email']) body.set("emailCC", formValues['Email']);
    body.set("formValues", JSON.stringify(formValues))
    body.set("sendEmailTo", sendEmailTo)

    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
    let options = new RequestOptions({ headers: headers });
    console.log(body);

    return this.http.post(url, body, options)
      .toPromise()
      .then(this.extractData)
      .catch(this.catchError);
  }

  private extractData(res: Response) {
    return res.json();
  }

  private catchError(error: Response | any): Promise<any> {
    // console.log(error);
    // return Observable.throw(error.json().error || error || "some error in http get");
    return Promise.reject(error.json() || error || "some error in http post");
  }

}
