import { Injectable } from '@angular/core';
import { Http, RequestOptions, URLSearchParams, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from "rxjs";
// const sgMail = require('@sendgrid/mail');
import * as sgMail from "@sendgrid/mail";


@Injectable()
export class EmailProvider {

  constructor(private http: Http) {
    console.log('Hello EmailProvider Provider');
  }

  sendEmail(formValues?) {
    console.log(formValues)
    let url = "https://api.mailgun.net/v3/sandboxbd02a290478a405590f5df1f415e883a.mailgun.org/messages"
    let header = new Headers({ 'Access-Control-Allow-Origin': '*', "Authorization": "Basic " + btoa("api:key-128655d1148482babc4f383b3f3c0595") })
    let testHtml = `<h4>Hi I'm Anas</h4><br><p>How are you</p>`
    let body = new URLSearchParams();
    body.set("from", "The BATMAN <anas@sandboxbd02a290478a405590f5df1f415e883a.mailgun.org>")
    body.set("to", "anas.munir.92@gmail.com")
    body.set("subject", "testing mailgun")
    // body.set("text", JSON.stringify(formValues))
    body.set("text", testHtml)

    let options = new RequestOptions({ headers: header })

    return this.http.post(url, body, options)
      .map(this.extractData)
      .catch(this.catchError)

  }

  sendGrid(formValues) {
    // sgMail.setApiKey("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");
    // const msg = {
    //   to: 'anas.munir.92@gmail.com',
    //   from: 'test@example.com',
    //   subject: 'Sending with SendGrid is Fun',
    //   text: 'and easy to do anywhere, even with Node.js',
    //   html: '<strong>and easy to do anywhere, even with Node.js</strong>',
    // };
    // sgMail.send(msg);
    let url = "http://formspree.io/anas.munir.92@gmail.com"
    return this.http.post(url, formValues)
  }

  private extractData(res: Response) {
    return res.json();
  }

  private catchError(error: Response | any) {
    console.log(error);
    return Observable.throw(error.json() || "Server error.")
  }
}
