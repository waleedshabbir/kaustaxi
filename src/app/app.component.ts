import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from "@ionic/storage";
import { GlobalVariables } from "./gloabls";


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any = 'LandingPage';

  constructor(
    platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    public globals: GlobalVariables,
    public storage: Storage) {

    this.getUser()
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }

  async getUser() {
    try {
      let user = await this.storage.get('user')
      if (user) {
        this.globals.user.email = user.email;
        this.globals.user.title = user.title;
        this.globals.user.name = user.name;
        this.globals.user.mobile = user.mobile;
        this.globals.user.phone = user.phone;
      }
      console.log('this.globals.user', this.globals.user)
    } catch (error) {
      console.error("error", error);
    }
  }
}

